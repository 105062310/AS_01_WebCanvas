# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

# Function
## Basic
* brush:
    當按下筆刷圖按的按鈕，若偵測到滑鼠按下，則開始根據滑鼠移動畫線，若滑鼠鍵放掉，則關掉偵測。
* eraser:
    由於已將畫布背景刷成白色，且在下載索賄圖片時，以jpg的格式存檔，所以利用白色刷子當作橡皮擦可以達到同樣效果。
* simple menu:
    * brush size:
        利用input type: range，用使用者選定的value來調整筆刷大小。
    * color:
        利用input type: color，根據使用者選定的顏色，透過onclick來偵測值的改變，進而更換畫筆顏色。
* text input:
    按下T圖樣的按鈕，展開輸入自、字體大小及字形選單，進行打字工作，若用完功能可再按一次Ｔ圖樣按鈕，收回功能表。
    * text:
        利用input及mousedown兩者的配合，來達到將使用者輸入的字放到canvas的效果。
    當輸入字到input處，由onchange這個變數去偵測輸入的東西是否有改變，若改變了則呼叫text()，將text_clicked設為true，告訴偵測mousedown的function使用者要用打字功能，然後當他在畫布按下滑鼠時，即會出現該字句。
    * text size/style:
        使用select設定了幾種字型及字體大小，當使用者選擇做改變，onchange會觸發相對應的function，更改size跟style。
* cursor icon:
    當按下不同按鈕，觸發不同function，於function中更改cursor，達到更換游標的效果。
* refresh button:
    按下reset觸發refresh功能，由於reset相當於重新開始畫畫，就直接連接init()，做與一開始用畫布的設定，將整個畫面刷白。
    
## Adavanced tool
* Different brush shapes:
    * circle/rectangle:
        運用js內建的function：arc()、rect()，來做主要畫圖形的工作，當按下相關按鈕，則將circle_clicked、rec_clicked設為true，這樣方便在偵測mousedown的工作中，讓程式了解他下一步要做什麼工作，又為了要做到滑鼠拖移圖按隨著增長的效果，每一個圓的公式可以由當初按下的起始點及現在滑鼠所在位子做運算得到，而問題在於因為每次滑鼠移動canvas都會偵測並畫圓，無法如想像中的只出現一個漸漸變大的圓，這時要利用toDataURL來記錄在畫圓之前canvas的樣子，每次畫新的圓之前都先重刷一次該圖在畫圓，即可達到所求效果，長方形亦同理。
    * triangl:
        由於三角形沒有內建好的function可以使用，所以利用畫線的功能，讓他在移動的時候跟隨游標畫出三條線，即可出現三角形，讓其畫面只出現跟隨游標增減的圖案方法如同圓形、長方形。
* Undo/Redo:
    運用array儲存每次改變畫布行為的網址，利用paintingStep選取不同次的網址，刷到畫布上，達到返回的效果。
* Image tool:
    利用input file type，存取電腦中的資料，再用與text相同的方法，根據滑鼠點擊，將圖片貼到畫布處。
* Download:
    存取畫布的網址，以jpg格式存入電腦。
