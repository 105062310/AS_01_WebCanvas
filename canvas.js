var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');

function init(){
  paintingPush();
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "white";
  ctx.fill();
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.stroke();
}

function circleMove(evt){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(circlePic, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.arc((startX+mousePos.x)/2, (startY+mousePos.y)/2, Math.sqrt(Math.pow(startX-mousePos.x, 2) + Math.pow(startY-mousePos.y, 2))/2, 0, 2*Math.PI);
  ctx.lineWidth = document.getElementById("myBrushSize").value;
  ctx.strokeStyle = document.getElementById("myColors").value;
  ctx.stroke();
}

function recMove(evt){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(recPic, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.rect(startX, startY, -(startX-mousePos.x), -(startY-mousePos.y));
  ctx.lineWidth = document.getElementById("myBrushSize").value;
  ctx.strokeStyle = document.getElementById("myColors").value;
  ctx.stroke();
}

function triMove(evt){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(triPic, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.moveTo(startX, startY);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.lineTo(startX-(mousePos.x-startX), mousePos.y);
  ctx.lineTo(startX, startY);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.lineWidth = document.getElementById("myBrushSize").value;
  ctx.strokeStyle = document.getElementById("myColors").value;
  ctx.stroke();
}

var startX, startY;
var circlePic = new Image();
var recPic = new Image();
var triPic = new Image();
canvas.addEventListener('mousedown', function(evt) {
  var mousePos = getMousePos(canvas, evt);
  startX = mousePos.x;
  startY = mousePos.y;
  //text
  if(text_clicked == true){
    ctx.font = document.getElementById("myTextSize").value + "px " + document.getElementById("myTextStyle").value;
    ctx.fillStyle = document.getElementById("myColors").value;
    ctx.fillText(document.getElementById("myText").value, mousePos.x, mousePos.y);
  }
  //circle
  else if(circle_clicked == true){
    circlePic.src = canvas.toDataURL();
    evt.preventDefault();
    canvas.addEventListener('mousemove', circleMove, false);
  }
  //rec
  else if(rec_clicked == true){
    recPic.src = canvas.toDataURL();
    evt.preventDefault();
    canvas.addEventListener('mousemove', recMove, false);
  }
  //tri
  else if(tri_clicked == true){
    triPic.src = canvas.toDataURL();
    evt.preventDefault();
    canvas.addEventListener('mousemove', triMove, false);
  }
  //photo
  else if(pic_clicked == true){
    ctx.drawImage(document.getElementById("temp_img"), mousePos.x, mousePos.y, 100, 120);
  }
  //brush
  else{
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  }
});

canvas.addEventListener('mouseup', function() {
  paintingPush();
  if(tri_clicked == true){
    ctx.closePath();
  }
  canvas.removeEventListener('mousemove', mouseMove, false);
  canvas.removeEventListener('mousemove', circleMove, false);
  canvas.removeEventListener('mousemove', recMove, false);
  canvas.removeEventListener('mousemove', triMove, false);
}, false);

//reset
document.getElementById('myReset').addEventListener('click', function() {
  init();
  text_clicked = false;
  photo_clicked = false;
  circle_clicked = false;
  rec_clicked = false;
  tri_clicked = false;
  pic_clicked = false;
  document.body.style.cursor = "default";
}, false);

//brush
document.getElementById('myBrush').addEventListener('click', function() {
    ctx.strokeStyle = document.getElementById("myColors").value;
    text_clicked = false;
    photo_clicked = false;
    circle_clicked = false;
    rec_clicked = false;
    tri_clicked = false;
    pic_clicked = false;
    document.body.style.cursor = "sw-resize";
}, false);

//color-change
function colorChange() {
  ctx.strokeStyle = document.getElementById("myColors").value;
}

//eraser
document.getElementById("myEraser").addEventListener('click', function() {
  ctx.strokeStyle = "white";
  text_clicked = false;
  photo_clicked = false;
  circle_clicked = false;
  rec_clicked = false;
  tri_clicked = false;
  pic_clicked = false;
  document.body.style.cursor = "alias";
  }, false);

//brushSize
function brushSize() {
  ctx.lineWidth = document.getElementById("myBrushSize").value;
}

//text_box
var text_i = 0;
var text_clicked = false;
var text = function(){ 
  if(text_i == 0){
    document.getElementById("myText").style.display = "block";
    document.getElementById("myTextStyle").style.display = "block";
    document.getElementById("myTextSize").style.display = "block";
    document.body.style.cursor = "text";
    text_i++;
  }else{
    document.getElementById("myText").style.display = "none";
    document.getElementById("myTextStyle").style.display = "none";
    document.getElementById("myTextSize").style.display = "none";
    document.getElementById("myText").value = "";
    document.body.style.cursor = "default";
    text_i--;
  }
  text_clicked = true;
}

//download
function download(link, canvasId, filename) {
  link.href = document.getElementById("art").toDataURL();
  link.download = filename;
}

document.getElementById('myDownload').addEventListener('click', function() {
  download(this, 'canvas', 'myPainting.jpg');
}, false);


//circle
var circle_clicked = false;
function circle(){
  circle_clicked = true;
  text_clicked = false;
  rec_clicked = false;
  tri_clicked = false;
  pic_clicked = false;
  document.body.style.cursor = "cell";
}

//rec
var rec_clicked = false;
function rec(){
  rec_clicked = true;
  circle_clicked = false;
  text_clicked = false;
  tri_clicked = false;
  pic_clicked = false;
  document.body.style.cursor = "cell";
}

//tri
var tri_clicked = false;
function tri(){
  tri_clicked = true;
  rec_clicked = false;
  circle_clicked = false;
  text_clicked = false;
  pic_clicked = false;
  document.body.style.cursor = "cell";
}

//pic
var pic_clicked = false;
function pic(){
  pic_clicked = true;
  tri_clicked = false;
  rec_clicked = false;
  circle_clicked = false;
  text_clicked = false;
  document.body.style.cursor = "grabbing";
}

//undo redo
var paintingArray = new Array();
var paintingStep = -1;

function paintingPush(){
  paintingStep++;
  if(paintingStep < paintingArray.length){
    paintingArray.length = paintingStep;  
  }
  var paintingNow = canvas.toDataURL();
  paintingArray.push(paintingNow);
}

function pUndo(){
  if(paintingStep > 0){
    paintingStep--;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var painting = new Image();
    painting.src = paintingArray[paintingStep];
    painting.onload =function(){ctx.drawImage(painting, 0, 0)};
  }
}

function pRedo(){
  if(paintingStep < paintingArray.length-1){
    paintingStep++;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var painting = new Image();
    painting.src = paintingArray[paintingStep];
    painting.onload = function(){ctx.drawImage(painting, 0, 0)};
  }
} 
